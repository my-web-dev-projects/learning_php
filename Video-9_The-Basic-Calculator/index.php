<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <input type="number" name="num01" placeholder="Input First Number">
        <br>
        <select name="operator">
            <option value="add">+</option>
            <option value="subtract">-</option>
            <option value="multiply">*</option>
            <option value="divide">/</option>
        </select>
        <br>
        <input type="number" name="num02" placeholder="Input Second Number">
        <br>
        <button>Calculate</button>
    </form>

    <?php 
    // Check if user has submit the form data correctly
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        // unsecure way = $num01 = $_POST["num01"];
        // sanitizing user data to avoid xss attacks
        $num01 = filter_input(INPUT_POST, "num01",FILTER_SANITIZE_NUMBER_FLOAT);
        $operator = filter_input(INPUT_POST, "operator",FILTER_SANITIZE_SPECIAL_CHARS);
        $num02 = filter_input(INPUT_POST, "num02",FILTER_SANITIZE_NUMBER_FLOAT);

        // Error Handlers
        $errors = false;

        if (empty($num01) || empty($operator) || empty($num02)){
            echo "<br><p>Please fill all the fields!</p>";
            $errors = true;
        }

        if (!is_numeric($num01) || !is_numeric($num02)){
            echo "<br><p>Please only add numbers!</p>";
            $errors = true;
        }

        // Calculate the numbers if no errors
        if(!$errors){
            // initializing value to $value variable
            $value = 0;
            switch ($operator){
                case "add":
                    $value = $num01 + $num02;
                    break;
                case "subtract":
                    $value = $num01 - $num02;
                    break;
                case "multiply":
                    $value = $num01 * $num02;
                    break;
                case "divide":
                    $value = $num01 / $num02;
                    break;
                default:
                    echo "<br><p>Something went wrong :(</p>";
            }

            echo "<p>Result = ". $value ."</p>";
        }
    }
    ?>
</body>
</html>
<?php
/*
Notes: video No. 6
-> $_SERVER and $_POST are Super Global in php
-> To protect our site from cross site scripting we will be using htmlspecialchars() function.
-> header() funcion is used to redirect user to the given location.
-> empty() funcion is used to check if the provided data field is empty or not (returns true or false).\
-> exit() Stops execution of the code.
*/

// var_dump($_SERVER["REQUEST_METHOD"]);

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    
    // Storing and sanitizing data
    $firstName = htmlspecialchars($_POST["firstname"]);
    $lastName = htmlspecialchars($_POST["lastname"]);
    $favPet = htmlspecialchars($_POST["favouritepet"]);

    // To make sure user fills required fields.
    if (empty($firstName)){
        exit();
        header("Location: ../index.php");
    }

    // Printing data on formhandler.php
    echo "Data";
    echo "<br>";
    echo $firstName;
    echo "<br>";
    echo $lastName;
    echo "<br>";
    echo $favPet;

    // redirecting user back to index.php
    header("Location: ../index.php");
} else {
    header("Location: ../index.php");
}
?>

<?php 

// Check whether user has clicked on submit button
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $username = $_POST["username"];
    $pwd = $_POST["pwd"];
    try{
        require_once "./dbh.inc.php";
        require_once "./mvc_login/login_model.inc.php";
        require_once "./mvc_login/login_contr.inc.php";

        // ERROR HANDLERS
        $errors = [];
        if(is_input_empty($username, $pwd)){
            $errors["empty_input"] = "Please fill out all fields.";
        }

        $result = get_user($pdo, $username);

        if (is_username_invalid($result)){
            $errors["login_incorrect"] = "Incorrect login info!";
        }

        if (!is_username_invalid($result) && is_pwd_invalid($pwd, $result["pwd"])){

        }

        require_once "config_session.inc.php";
        
        if ($errors){
            $_SESSION["errors_login"] = $errors;

            header("Location: ../index.php");
            die();
        }
        
        // Update Session ID after login
        $newSessionId = session_create_id();
        $sessionId = $newSessionId . "_" . $result["id"];
        session_id($sessionId);

        $_SESSION["user_id"] = $result["id"];
        $_SESSION["user_username"] = htmlspecialchars($result["username"]);

        // reset session regeneration time
        $_SESSION['last_regeneration'] = time();

        header("Location: ../index.php?login=success");
        $pdo = null;
        $stmt = null;
        die();

    } catch (PDOException $error){
        die("Query failed: ". $error->getMessage());
    }
}
else{
        // Send user back to index.php if he/she is trying to access this page
        header("Location: ../index.php");
        die();
}
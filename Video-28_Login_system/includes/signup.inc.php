<?php

// Check whether user has clicked on submit button
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $username = $_POST["username"];
    $pwd = $_POST["pwd"];
    $email = $_POST["email"];

    try{
        require_once "dbh.inc.php";
        require_once "mvc_signup/signup_model.inc.php";
        require_once "mvc_signup/signup_contr.inc.php";

        // ERROR HANDLERS
        $errors = [];
        if(is_input_empty($username, $pwd, $email)){
            $errors["empty_input"] = "Please fill out all fields.";
        }
        if (is_email_invalid($email)){
            $errors["invalid_email"] = "Please enter valid E-mail.";
        }

        if (is_username_taken($pdo, $username)){
            $errors["username_taken"] = "Username already taken.";
        }
        if (is_email_registered($pdo, $email)){
            $errors["email_userd"] = "E-mail already registered";
        }

        require_once "config_session.inc.php";
        
        if ($errors){
            $_SESSION["errors_signup"] = $errors;

            $signupData = [
                "username" => $username,
                "email" => $email
            ];

            $_SESSION["signup_data"] = $signupData;

            header("Location: ../index.php");
            die();
        }

        create_user($pdo, $username, $pwd, $email);

        header("Location: ../index.php?signup=success");
        $pdo = null;
        $stmt = null;
        die();
    } catch (PDOException $error){
        die("Query failed: ". $error->getMessage());
    }

} else{
    // Send user back to index.php if he/she is trying to access this page
    header("Location: ../index.php");
    die();
}
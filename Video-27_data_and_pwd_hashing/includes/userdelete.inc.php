<?php

// Check whether user has clicked on submit button
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $username = $_POST["username"];
    $pwd = $_POST["pwd"];

    try{
        require_once "dbh.inc.php";

        $query = "DELETE FROM users WHERE username = :username AND pwd = :pwd;";

        $stmt = $pdo->prepare($query);

        $stmt->bindParam(":username",$username);
        $stmt->bindParam(":pwd",$pwd);

        $stmt->execute();

        $pdo = null;
        $stmt = null;

        header("Location: ../index.php");
        die();
    } catch (PDOExecption $error){
        die("Query failed: ". $error->getMessage());
    }

} else{
    // Send user back to index.php if he/she is trying to access this page
    header("Location: ../index.php");
}
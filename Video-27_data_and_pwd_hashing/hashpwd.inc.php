<?php

//  Basic Hash to secure Data

$sensitiveData = "MEOW World!"; // user input
$salt = bin2hex(random_bytes(16)); //Generate random salt
$pepper = "ASecretPepperString";

$data2Hash = $sensitiveData . $salt . $pepper;
$hashed = hash("sha256", $data2Hash);

/* How to verify the hashed data with what user has entered */

$sensitiveData = "MEOW World!"; // user input

$storedSalt = $salt; // fetched from DB
$storedHash = $hashed; // fetched from DB
$pepper = "ASecretPepperString";

$data2Hash = $sensitiveData . $storedSalt . $pepper;

$verificationHash = hash("sha256", $data2Hash);

if ($storedHash === $verificationHash){
    echo "The data are the same!";
}else{
    echo "The data are not the same!";
}

// Logic for password

$pwdSignup = "Krossing";

$options = [
    'cost' => 12
];

$hashedPwd = password_hash($pwdSignup, PASSWORD_BCRYPT, $options);

// How to verify

$pwdLogin = "Krossing";

if (password_verify($pwdLogin, $hashedPwd)){
    echo "They are the same!";
} else {
    echo "They are not the same!";
}
